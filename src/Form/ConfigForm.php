<?php

namespace Drupal\devmode\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'devmode.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'devmode.configform';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#tree' => TRUE,
    ];

    $form['general']['activate_devmode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate developer mode'),
      '#description' => $this->t('Check this if you want to activate the developer mode.'),
      '#default_value' => $this->config('devmode.settings')->get('active'),
    ];

    $form['general']['preferred_dumper_method'] = [
      '#type' => 'select',
      '#title' => $this->t()
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = $form_state->getValues();

    $this->config('devmode.settings')->set('active', $formValues['general']['activate_devmode']);
  }

  /**
   * @return array
   */
  private function getAvailableDumper() : array {

  }
}
