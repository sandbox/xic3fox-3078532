<?php

namespace Drupal\devmode;

use Drupal;

class Dumper {

  /**
   * Search for available dumper methods.
   *
   * @return array
   *   The actual usable dumpers.
   */
  public function getAvailableDumpers() {
    $possibleDumpers = [
      'var_dump',
      'dump',
      'kint',
    ];

    $dumpers = [];

    foreach ($possibleDumpers as $possibleDumper) {
      if (function_exists($possibleDumper)) {
        $dumpers[] = $possibleDumper;
      }
    }

    return $dumpers;
  }

  /**
   *
   * @param $var
   * @param mixed ...$vars
   */
  public function dump($var, ...$vars) {
    $config = Drupal::config('devmode.settings');

    // Get the configured preferred dumper method.
    $preferredDumperMethod = $config->get('preferred_dumper_method');

    // Check if the developer mode is active at all.
    $devModeIsActive = (bool) $config->get('active');

    // Return if dev mode is inactive to save indentations.
    if (!$devModeIsActive) {
      return;
    }

    // Dump the single $arg first.
    $preferredDumperMethod($var);

    // Iterate over $vars to dump every passed arg.
    foreach ($vars as $var) {
      $preferredDumperMethod($var);
    }
  }
}
